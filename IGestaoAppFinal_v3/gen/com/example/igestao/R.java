/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.example.igestao;

public final class R {
    public static final class anim {
        public static final int polaris__grow_fade_in_from_bottom=0x7f040000;
        public static final int polaris__shrink_fade_out_to_bottom=0x7f040001;
    }
    public static final class attr {
    }
    public static final class color {
        public static final int polaris__primary=0x7f060000;
        public static final int polaris__secondary=0x7f060001;
    }
    public static final class dimen {
        public static final int polaris__font_size_normal=0x7f070004;
        public static final int polaris__font_size_small=0x7f070003;
        public static final int polaris__spacing_large=0x7f070002;
        public static final int polaris__spacing_normal=0x7f070001;
        public static final int polaris__spacing_small=0x7f070000;
    }
    public static final class drawable {
        public static final int car_icon=0x7f020000;
        public static final int email_icon=0x7f020001;
        public static final int gpin=0x7f020002;
        public static final int grupo_big=0x7f020003;
        public static final int ic_action_search=0x7f020004;
        public static final int ic_launcher=0x7f020005;
        public static final int ic_launcher_polaris=0x7f020006;
        public static final int loading=0x7f020007;
        public static final int logo_erisa=0x7f020008;
        public static final int logo_isg=0x7f020009;
        public static final int logo_ulht=0x7f02000a;
        public static final int logo_ulp=0x7f02000b;
        public static final int logot_grupo=0x7f02000c;
        public static final int map_pin_holed_blue=0x7f02000d;
        public static final int map_pin_holed_blue_alt=0x7f02000e;
        public static final int map_pin_holed_blue_normal=0x7f02000f;
        public static final int map_pin_holed_violet=0x7f020010;
        public static final int map_pin_holed_violet_alt=0x7f020011;
        public static final int map_pin_holed_violet_normal=0x7f020012;
        public static final int phone_icon=0x7f020013;
        public static final int pin=0x7f020014;
        public static final int polaris__arrow_to_right=0x7f020015;
        public static final int polaris__btn_map_overlay=0x7f020016;
        public static final int polaris__btn_map_overlay_focused=0x7f020017;
        public static final int polaris__btn_map_overlay_normal=0x7f020018;
        public static final int polaris__btn_map_overlay_pressed=0x7f020019;
        public static final int polaris__ic_map_locate=0x7f02001a;
        public static final int polaris__map_callout_bottom_anchor=0x7f02001b;
        public static final int polaris__map_callout_bottom_anchor_focused=0x7f02001c;
        public static final int polaris__map_callout_bottom_anchor_normal=0x7f02001d;
        public static final int polaris__map_callout_bottom_anchor_pressed=0x7f02001e;
        public static final int polaris__map_callout_left_cap=0x7f02001f;
        public static final int polaris__map_callout_left_cap_focused=0x7f020020;
        public static final int polaris__map_callout_left_cap_normal=0x7f020021;
        public static final int polaris__map_callout_left_cap_pressed=0x7f020022;
        public static final int polaris__map_callout_right_cap=0x7f020023;
        public static final int polaris__map_callout_right_cap_focused=0x7f020024;
        public static final int polaris__map_callout_right_cap_normal=0x7f020025;
        public static final int polaris__map_callout_right_cap_pressed=0x7f020026;
        public static final int pushpin=0x7f020027;
        public static final int splash=0x7f020028;
        public static final int splashig=0x7f020029;
        public static final int splashnew=0x7f02002a;
    }
    public static final class id {
        public static final int btnEmail=0x7f0a0009;
        public static final int btnMap=0x7f0a0007;
        public static final int btnNav=0x7f0a0008;
        public static final int btnNot=0x7f0a0006;
        public static final int btnTlf=0x7f0a000a;
        public static final int btnULHT=0x7f0a0002;
        public static final int btnULP=0x7f0a0003;
        public static final int btnVid=0x7f0a0005;
        public static final int btnWeb=0x7f0a0004;
        public static final int desc=0x7f0a000d;
        public static final int imageView1=0x7f0a0001;
        public static final int link=0x7f0a000f;
        public static final int map_view_container=0x7f0a0012;
        public static final int polaris__callout=0x7f0a0013;
        public static final int polaris__content=0x7f0a0015;
        public static final int polaris__content_container=0x7f0a0014;
        public static final int polaris__disclosure=0x7f0a0018;
        public static final int polaris__subtitle=0x7f0a0017;
        public static final int polaris__title=0x7f0a0016;
        public static final int pub=0x7f0a000e;
        public static final int title=0x7f0a000c;
        public static final int userVideoThumbImageView=0x7f0a0010;
        public static final int userVideoTitleTextView=0x7f0a0011;
        public static final int videosListView=0x7f0a000b;
        public static final int webView1=0x7f0a0000;
    }
    public static final class interpolator {
        public static final int polaris__decelerate_cubic=0x7f050000;
        public static final int polaris__decelerate_quint=0x7f050001;
    }
    public static final class layout {
        public static final int accessory=0x7f030000;
        public static final int activity_igweb=0x7f030001;
        public static final int activity_local=0x7f030002;
        public static final int activity_main=0x7f030003;
        public static final int activity_maps=0x7f030004;
        public static final int activity_noticias=0x7f030005;
        public static final int activity_splash1=0x7f030006;
        public static final int activity_video=0x7f030007;
        public static final int linhafeed=0x7f030008;
        public static final int linhavideo=0x7f030009;
        public static final int list_item_user_video=0x7f03000a;
        public static final int loc_erisa=0x7f03000b;
        public static final int loc_isg=0x7f03000c;
        public static final int loc_ulht=0x7f03000d;
        public static final int loc_ulp=0x7f03000e;
        public static final int mapapolaris=0x7f03000f;
        public static final int polaris__map_callout_view_merge=0x7f030010;
        public static final int polaris__user_tracking_button=0x7f030011;
    }
    public static final class string {
        public static final int app_name=0x7f080002;
        public static final int exit_press_back_twice_message=0x7f080014;
        public static final int m_mapa=0x7f08000e;
        public static final int m_not=0x7f080010;
        public static final int m_vid=0x7f08000f;
        public static final int menu_settings=0x7f080003;
        public static final int mweb=0x7f08000d;
        public static final int polaris__locate_myself=0x7f080000;
        public static final int polaris__unable_to_locate_you=0x7f080001;
        public static final int site=0x7f08000b;
        public static final int spscreen=0x7f08000c;
        public static final int text_email=0x7f080012;
        public static final int text_nav=0x7f080011;
        public static final int text_tlf=0x7f080013;
        public static final int title_activity_contemail=0x7f080009;
        public static final int title_activity_conttlf=0x7f08000a;
        public static final int title_activity_igestao=0x7f080004;
        public static final int title_activity_main=0x7f080005;
        public static final int title_activity_maps=0x7f080007;
        public static final int title_activity_noticias=0x7f080008;
        public static final int title_activity_splash1=0x7f080006;
    }
    public static final class style {
        public static final int AppTheme=0x7f090004;
        public static final int SplashScreen=0x7f090005;
        public static final int TextAppearance_Polaris=0x7f090000;
        public static final int TextAppearance_Polaris_MapCallout=0x7f090001;
        public static final int TextAppearance_Polaris_MapCallout_Subtitle=0x7f090003;
        public static final int TextAppearance_Polaris_MapCallout_Title=0x7f090002;
    }
}
