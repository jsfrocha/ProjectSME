package com.example.igestao;


import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.example.igestao.R;


@SuppressLint("SetJavaScriptEnabled")
public class IGWeb extends Activity {
	
	
	ProgressDialog mProgress; //Inicializacao do Progress Dialog
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_igweb);
		WebView mWeb = (WebView)this.findViewById(R.id.webView1);
		
		WebSettings settings = mWeb.getSettings();
		settings.setJavaScriptEnabled(true);
		
		mProgress = ProgressDialog.show(this, "Loading", "Espere um pouco.."); //Conteudo do Progress Dialog
		
		
		//Inicializar um WebViewClient para ser possivel mostrar o Progress Dialog, e onPageFinished, retirar o Dialog
		mWeb.setWebViewClient(new WebViewClient() {
			
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.loadUrl(url);
				return true;
			}
			
			public void onPageFinished(WebView view, String url) {
				if (mProgress.isShowing()) {
					mProgress.dismiss();
				}
			}
		});
		
	mWeb.loadUrl( getString(R.string.site) );
	}
                      
            
            
            


    }
    
    
    
    


