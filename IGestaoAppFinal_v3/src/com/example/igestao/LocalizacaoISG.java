package com.example.igestao;


import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.app.Activity;
import android.content.Intent;

import com.example.igestao.R;



public class LocalizacaoISG extends Activity {
	
	Button btn;
	Button btn2;
	Button btn3;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loc_isg);
        btn = (Button)findViewById(R.id.btnNav);
        btn2 = (Button)findViewById(R.id.btnEmail);
        btn3 = (Button)findViewById(R.id.btnTlf);
        
        btn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?daddr=38.746861,-9.100412"));
				intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
				startActivity(intent);
			}
		
        		
		});
        btn2.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//Contactar por email
				Uri email = Uri.parse("mailto:informacoes@isg.pt");
				Intent intent = new Intent(Intent.ACTION_VIEW,email);
				startActivity(intent);
				
				
			}
		});
        
        btn3.setOnClickListener(new View.OnClickListener() {
			//Contactar por telefone
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent tlf = new Intent (Intent.ACTION_DIAL);
				tlf.setData(Uri.parse("tel:217513700 "));
				startActivity(tlf);
				
				
			}
		});
	
	

}
    
}

