package com.example.igestao;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;

import java.util.ArrayList;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import android.widget.Toast;



import com.example.igestao.Config;
import com.cyrilmottier.polaris.Annotation;
import com.cyrilmottier.polaris.MapCalloutView;
import com.cyrilmottier.polaris.MapViewUtils;
import com.cyrilmottier.polaris.PolarisMapView;
import com.cyrilmottier.polaris.PolarisMapView.OnAnnotationSelectionChangedListener;
import com.cyrilmottier.polaris.PolarisMapView.OnRegionChangedListener;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;

public class MainMapActivity extends MapActivity implements OnRegionChangedListener, OnAnnotationSelectionChangedListener {

    private static final String LOG_TAG = "MainActivity";

    //@formatter:off
    private static final Annotation[] sLusofona = {
        new Annotation(new GeoPoint((int)(38.757982 * 1E6),(int)(-9.153193 * 1E6)), "ULHT", "Universidade Lusofona de Humanidades e Tecnologias"),
        new Annotation(new GeoPoint((int)(41.14334 * 1E6),(int)(-8.608134 * 1E6)), "ULP", "Universidade Lusofona do Porto"),
        new Annotation(new GeoPoint((int)(38.781386 * 1E6),(int)(-9.16079 * 1E6)), "ERISA", "Escola Superior de Saude Ribeiro Sanches"),
        new Annotation(new GeoPoint((int)(38.746861 * 1E6),(int)(-9.100412 * 1E6)), "ISG", "Instituto Superior de Gest�o"),
    };
    
    private MapController mc;
    

   
    
    private static final Annotation[][] sRegions = {sLusofona};
    //@formatter:on

    private PolarisMapView mMapView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.mapapolaris);

        mMapView = new PolarisMapView(this, "01dF72Ifcv0JeEMrz1UTP6xMjEsgnal_psrtI7A");
        mMapView.setUserTrackingButtonEnabled(true);
        mMapView.setOnRegionChangedListenerListener(this);
        mMapView.setOnAnnotationSelectionChangedListener(this);
        mc = mMapView.getController();
        mc.setZoom(7);
        mc.animateTo(new GeoPoint((int)(38.757982 * 1E6),(int)(-9.153193 * 1E6)));

        // Prepare an alternate pin Drawable
        final Drawable altMarker = MapViewUtils.boundMarkerCenterBottom(getResources().getDrawable(R.drawable.map_pin_holed_violet));

        // Prepare the list of Annotation using the alternate Drawable for all
        // Annotation located in France
        final ArrayList<Annotation> annotations = new ArrayList<Annotation>();
        for (Annotation[] region : sRegions) {
            for (Annotation annotation : region) {
                if (region == sLusofona) {
                    annotation.setMarker(altMarker);
                }
                annotations.add(annotation);
            }
        }
        mMapView.setAnnotations(annotations, R.drawable.map_pin_holed_blue);

        final FrameLayout mapViewContainer = (FrameLayout) findViewById(R.id.map_view_container);
        mapViewContainer.addView(mMapView, new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
    }

    @Override
    protected void onStart() {
        super.onStart();
        mMapView.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mMapView.onStop();
    }

    @Override
    protected boolean isRouteDisplayed() {
        return false;
    }

    @Override
    public void onRegionChanged(PolarisMapView mapView) {
        if (Config.INFO_LOGS_ENABLED) {
            Log.i(LOG_TAG, "onRegionChanged");
        }
    }

    @Override
    public void onRegionChangeConfirmed(PolarisMapView mapView) {
        if (Config.INFO_LOGS_ENABLED) {
            Log.i(LOG_TAG, "onRegionChangeConfirmed");
        }
    }

    @Override
    public void onAnnotationSelected(PolarisMapView mapView, MapCalloutView calloutView, int position, Annotation annotation) {
        if (Config.INFO_LOGS_ENABLED) {
            Log.i(LOG_TAG, "onAnnotationSelected");
        }
        calloutView.setDisclosureEnabled(true);
        calloutView.setClickable(true);
        if (!TextUtils.isEmpty(annotation.getSnippet())) {
            calloutView.setLeftAccessoryView(getLayoutInflater().inflate(R.layout.accessory, calloutView, false));
        } else {
            calloutView.setLeftAccessoryView(null);
        }
    }

    @Override
    public void onAnnotationDeselected(PolarisMapView mapView, MapCalloutView calloutView, int position, Annotation annotation) {
        if (Config.INFO_LOGS_ENABLED) {
            Log.i(LOG_TAG, "onAnnotationDeselected");
        }
    }

    @Override
    public void onAnnotationClicked(PolarisMapView mapView, MapCalloutView calloutView, int position, Annotation annotation) {
        if (Config.INFO_LOGS_ENABLED) {
            Log.i(LOG_TAG, "onAnnotationClicked");
        }
        
 
        
        if (annotation.getTitle() == "ULHT") {
        	
			Intent intent = new Intent(this, LocalizacaoULHT.class);
			startActivity(intent);
        }
        if (annotation.getTitle() == "ULP"){
			Intent intent = new Intent(this, LocalizacaoULP.class);
			startActivity(intent);
        }
        
        if (annotation.getTitle() == "ERISA"){
			Intent intent = new Intent(this, LocalizacaoERISA.class);
			startActivity(intent);
        }
        
        if (annotation.getTitle() == "ISG"){
			Intent intent = new Intent(this, LocalizacaoISG.class);
			startActivity(intent);
        }
        
        //Toast.makeText(this, getString(R.string.annotation_clicked, annotation.getPoint()), Toast.LENGTH_SHORT).show();
    }

}

