package com.example.igestao;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.net.URL;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import org.apache.http.ParseException;


public class Noticias extends ListActivity {
	
	// URL da FEED
	static final String URL = "http://loc.grupolusofona.pt/index.php/?format=feed";
	
	// XML node keys - Identificadores dos Nodes no XML
	static final String KEY_ITEM = "item"; // parent node

	static final String KEY_TITLE = "title";
	static final String KEY_DESC = "description";
	static final String KEY_LINK = "link";
	static final String KEY_PUBDATE = "pubDate";
	ProgressDialog progressDialog;
	ArrayList<HashMap<String, Spanned>> menuItems = new ArrayList<HashMap<String, Spanned>>();
	private DownloadFilesTask download;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_noticias);
		
		download = new DownloadFilesTask(Noticias.this);
		download.execute();
		
		

	}
	
	
	
	private class DownloadFilesTask extends AsyncTask<URL, Integer, Long> {
		private Context context;
		public DownloadFilesTask (Context context){
			this.context = context;
		}
		
		protected void onPreExecute() {
			progressDialog = ProgressDialog.show(context, "Loading", "Carregando Noticias", true);
		}
		
		protected Long doInBackground(URL... urls) {
			
			XMLParser parser = new XMLParser(); // Inicializacao do Parser
			String xml = parser.getXmlFromUrl(URL); // Obter o XML
			Document doc = parser.getDomElement(xml); // Obter elemento DOM

			NodeList nl = doc.getElementsByTagName(KEY_ITEM);
			// Loop por todos os nodes <item> (onde come�a cada noticia) 
			
			
			//String para guardar pubdates, para mais tarde comparar com data actual
			String data[] = new String[nl.getLength()];
			
			
			
			
			// nl.getLength = numero de noticias existentes (blocos de <item>)
					
			for (int i = 0; i < nl.getLength(); i++) {
				
				// Cria��o do HashMap que vai conter cada noticia
				HashMap<String, Spanned> map = new HashMap<String, Spanned>();
				
				//Identifica��o de 'e' como o Elemento que vai conter os <item> � medida que o ciclo vai incrementando
				Element e = (Element) nl.item(i);
				
				//Inserir pubdates na string para convers�o
				data[i] = getTagValue("pubDate", e);
				
				//Convers�o da PubDate
				SimpleDateFormat sdf = new SimpleDateFormat("EE, dd MMM yyyy HH:mm:ss z", Locale.UK);
				Date date = null;
				
				try {
					date = sdf.parse(data[i]); //Indicar de onde vem a string que contem a data
					String timeOfDay = new SimpleDateFormat("HH:mm").format(date); // String para ser introduzida no output
					java.sql.Timestamp timeStampDate = new Timestamp(date.getTime());
					java.sql.Timestamp timeStampNow = new Timestamp((new java.util.Date()).getTime()); //Obter a hora actual 
					
					long secondDiff = timeStampNow.getTime() / 1000 - timeStampDate.getTime() / 1000;
					int minuteDiff = (int) (secondDiff / 60);
					int hourDiff = (int) (secondDiff / 3600);
					int dayDiff = daysBetween(date, new Date()); //M�todo que faz a diferen�a entre hora actual e hora de publicacao
					if (dayDiff > 0) {
						System.out.println("H� " + dayDiff + " dia(s), �s " + timeOfDay); //Para verifica��o em LogCat se est� a fazer a convers�o correcta
						data[i] = ("H� " + dayDiff + " dia(s), �s " + timeOfDay);// Inserir na string data[] a nova string
								
						
					} else if (hourDiff > 0) {
						System.out.println("H� " + hourDiff + " hora(s), �s " + timeOfDay);
						data[i] = ("H� " + hourDiff + " hora(s), �s " + timeOfDay);
				
						
					} else if (minuteDiff > 0) {
						System.out.println("H� " + minuteDiff + " minuto(s), �s " + timeOfDay);
						data[i] = ("H� " + minuteDiff + " minuto(s), �s " + timeOfDay);
						
						
					} else if (secondDiff > 0) {
						System.out.println("H� " + secondDiff + " segundo(s), �s " + timeOfDay);
						data[i] = ("H� " + secondDiff + " segundo(s), �s " + timeOfDay);
						
					}
				} catch (ParseException e1) {
					e1.printStackTrace();
				} catch (java.text.ParseException e1) {
					e1.printStackTrace();
				}
				
				
				// Adicionar cada Child Node ao HashMap (Key -> Value) <String, Spanned> para apanhar a <description> com o CDATA
				
				map.put(KEY_TITLE, Html.fromHtml(parser.getValue(e, KEY_TITLE)));
				map.put(KEY_DESC, Html.fromHtml(parser.getValue(e, KEY_DESC)));
				map.put(KEY_PUBDATE, Html.fromHtml(data[i]));
				map.put(KEY_LINK, Html.fromHtml(parser.getValue(e, KEY_LINK)));

				// Adicionar o HashMap ao ArrayList
				menuItems.add(map);
			}//endFOR
			return null;
			
		}
		
		protected void onPostExecute(Long result) {
			progressDialog.dismiss();
			adapter_listview();

			
		}
	}
	
	
	




	
	private static String getTagValue(String sTag, Element e) { // M�todo para obter o valor de um node, neste caso utilizado para 'pubDate'
		
		NodeList nlList = e.getElementsByTagName(sTag).item(0).getChildNodes();
		
		Node nValue = (Node) nlList.item(0);
		
		return nValue.getNodeValue();
		
	}
	
	public static int daysBetween(Date startDate, Date endDate) { // M�todo para calcular a diferen�a de dias/horas/minutos/segundos na data
		
		int daysBetween = 0;
		while (startDate.before(endDate)) {
			startDate.setTime(startDate.getTime() + 86400000);
			daysBetween++;
		}
		return daysBetween;
		
	}
	
	public void adapter_listview(){
		
		ListAdapter adapter = new SimpleAdapter(this, menuItems, R.layout.linhafeed, 
				new String[] { KEY_TITLE, KEY_DESC, KEY_PUBDATE, KEY_LINK },
				new int[] { R.id.title, R.id.desc, R.id.pub, R.id.link });
		
		//Inserir ListView
		setListAdapter(adapter);
		
		ListView lv = getListView();
		
		// Que ao clicar...
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				// Vai buscar o valor de 'link' na respectiva TextView
				String link = ((TextView) view.findViewById(R.id.link)).getText().toString();
				
				// (Verifica��o em LogCat se ficou bem carregado o link)
				System.out.println("Link: " + link);

				// Novo Intent para abrir um website com o link da noticia respectiva
				Intent in = new Intent(Intent.ACTION_VIEW);
				in.setData(Uri.parse(link));

				startActivity(in);

			}
		});
		
	}
}
