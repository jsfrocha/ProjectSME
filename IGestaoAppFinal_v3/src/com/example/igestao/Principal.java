package com.example.igestao;


import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.*;

import com.example.igestao.R;
import com.example.igestao.*;



@SuppressLint("SetJavaScriptEnabled")
public class Principal extends Activity {
	
	Button btn;
	Button btn2;
	Button btn3;
	Button btn4;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn = (Button)findViewById(R.id.btnWeb);
        btn2 = (Button)findViewById(R.id.btnMap);
        btn3 = (Button)findViewById(R.id.btnVid);
        btn4 = (Button)findViewById(R.id.btnNot);
        
        btn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(Principal.this, IGWeb.class);
				startActivity(intent);
			}
		
        		
		});
        btn2.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(Principal.this, MainMapActivity.class);
				startActivity(intent);
				
				
			}
		});
        
        
        btn3.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(Principal.this, MainVideoActivity.class);
				startActivity(intent);
				
				
			}
		
        		
		});
        
        btn4.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(Principal.this, Noticias.class);
				startActivity(intent);
			}
        
        });
        
            
            


    }
    
    private boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onResume() {
        super.onResume();
        // .... other stuff in my onResume ....
        this.doubleBackToExitPressedOnce = false;
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, R.string.exit_press_back_twice_message, Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
             doubleBackToExitPressedOnce=false;   

            }
        }, 2000);
    } 

    
    
}

