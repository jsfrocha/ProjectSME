package com.example.igestao;

import com.example.igestao.R;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;

public class Splash extends Activity {
	
	// Tempo que o SplashScreen est� activo
	private final int SPLASH_DISPLAY_LENGTH = 5000;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash1);
	}
	
	@Override
	protected void onResume()
	{
		super.onResume();
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
		// Obtain the sharedPreference, default to true if not available
		boolean isSplashEnabled = sp.getBoolean("isSplashEnabled", true);
		
		if (isSplashEnabled)
		{
			new Handler().postDelayed(new Runnable() //Runnable que � atrasado pelo SPLASH_DISPLAY_LENGTH
			{
				@Override
				public void run()
				{
					//Finish � activity Splash para que ao pressionar Back, n�o volte ao Splash
					Splash.this.finish();
					//Intent para come�ar a activity main (Ecran Inicial)
					Intent mainIntent = new Intent(Splash.this, Principal.class);
					Splash.this.startActivity(mainIntent);
					
				}
			}, SPLASH_DISPLAY_LENGTH);
		}
		else
		{
			//Se o Splash j� n�o esta activo, ir para o menu inicial (redundancia)
			finish();
			Intent mainIntent = new Intent(Splash.this, Principal.class);
			Splash.this.startActivity(mainIntent);
			
		}
	}
}

