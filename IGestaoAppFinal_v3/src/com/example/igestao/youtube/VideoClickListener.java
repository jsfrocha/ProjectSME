package com.example.igestao.youtube;

public interface VideoClickListener {
	public void onVideoClicked(Video video);
}
