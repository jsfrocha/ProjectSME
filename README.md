João Rocha - ProjectSME 
========================

Android Application - Academic Project
---------------------------------------

The presented application uses Java to retrieve various types of information from Grupo Lusófona webpages and social media pages.

Goals
-----

1. Retrieve daily news from Grupo Lusófona's RSS Feed;
2. Present a map containing the locations of Grupo Lusófona's buildings in Portugal;
  * Make calls, send e-mails and provide driving directions.
3. Fetch videos from Grupo Lusófona's Youtube profile;

Solutions
---------

1. Asynchronously ran a XML Parser to retrieve the information from the Feed, loading it on a clickable ListView when available;
2. Used Google Maps API and Polaris Map Library to present a interactive map in a MapView containing the geo-locations of the said buildings;
  * Used Intents to open the various native applications needed to fulfill those tasks.
3. Videos were retrieved asynchronously through the Youtube API and loaded on a ListView when available.





